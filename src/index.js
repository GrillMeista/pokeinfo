console.log("LOADED INDEX.JS");

const weaknesses = getData();
const search = document.querySelector('#search');
const list = document.querySelector('#myList');

//weaknesses[0].name 

const searchFn = (e) => {
    addToList(e.target.value);
};

function addToList(value)
{
    let element = document.createElement('li');
    element.value = value;
    element.textContent = value;

    list.appendChild(element);
}

search.addEventListener('input', searchFn);